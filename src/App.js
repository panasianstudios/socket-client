// import packages
import React, { Component } from 'react';
import socketIOClient from 'socket.io-client';

// making the app component
class App extends Component {
  constructor() {
    super();

    this.state = {
      endpoint: "http://192.168.10.33:4001/", // target connection for socket
      color: 'white'
    };
  }

  // sending sockets
  send = () => {
    const socket = socketIOClient(this.state.endpoint);
    socket.emit('change color', this.state.color);
  }

  // adding the function
  setColor = (color) => (
    this.setState({ color })
  )

  // render method that renders in code if the state is updated
  render() {
    const socket = socketIOClient(this.state.endpoint);

    socket.on('change color', (color) => {
      document.body.style.backgroundColor = color;
    });

    return (
      <div style={{ textAlign: "center" }}>
        <button onClick={() => this.send() }>Change Color</button>
        <button id="blue" onClick={() => this.setColor('blue')}>Blue</button>
        <button id="red" onClick={() => this.setColor('red')}>Red</button>
      </div>
    );
  }
}

export default App;